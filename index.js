//@ts-check
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
const server = new (require('http').Server)(app);
const WebSocket = require('ws');
const fetch = require('node-fetch')




const port = process.env.PORT || 3000;
const tickerWss = 'wss://engine2.kryptono.exchange/ws/v1/tk'
const pairDetailPath = 'https://engine2.kryptono.exchange/api/v1/dp'
const openOrders = 'https://kryptono.exchange/k/order/total_open_order'
const p2pVolumnAPI = 'https://kryptono.exchange/k/p2p/trade/transaction/analytics'


/** @type {{[k:string] : PairInfo}}*/
let pairsInfo = {}

/** @type {WSTicker}*/
let globalData = {}


/** @type {{[k:string] : {success:number}}*/
let volumeP2P = {}

let lastUpdate = 0
let updateTask = null;
let lastTicker = 0;

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/v1/getmarketsummaries', async function (req, res) {

  if (Date.now() - lastUpdate > 5000) {
    updateTask = updateTask || getAllPairs(globalData.d.map(e => e.s));
    await updateTask;
    lastUpdate = Date.now();
  }
  updateTask = null;


  res.json(mapData(globalData))
})

app.get('/v1/totalvolumes', async function (req, res) {

  if (Date.now() - lastUpdate > 5000) {
    updateTask = updateTask || getAllPairs((globalData.d || []).map(e => e.s));
    await updateTask;
    lastUpdate = Date.now();
  }
  updateTask = null;

  res.json(mapVolume(globalData))
})


app.get('/v1/fiatexchangerate', require('./exchangeRate'));
app.get('/v1/usdtticker', require('./externalTicker'));
app.get('/v1/ex_depth', require('./externalDepth'));
app.post('/v1/payment_detail', require('./externalPayment'));


app.get('/up', async function (req, res) {
  res.send("up\n\n")
})

require('./socketforward')(server);


function getTotalOrders() {
  return fetch(openOrders)
    .then(e => e.json())
    .then(total_open_order => {
      for (let { _id, total_buy, total_sell } of total_open_order)
        pairsInfo[_id] = Object.assign(pairsInfo[_id] || {}, {
          total_buy,
          total_sell,
        })
    })
}

function getP2PVolume() {
  return fetch(p2pVolumnAPI)
    .then(e => e.json())
    .then(e => volumeP2P = e)
}




async function getPairDetail(pairStr) {
  return fetch(`${pairDetailPath}?symbol=${pairStr}`)
    .then(e => e.json())
    .then(e => {
      console.log("DONE", pairStr)
      pairsInfo[pairStr] = Object.assign(pairsInfo[pairStr] || {}, e)
    })
}


async function getAllPairs(pairs) {
  console.log("GET ALLPAIRS API")

  return Promise.all([
    ...pairs.map(getPairDetail),
    getP2PVolume(),
    // getTotalOrders(),
  ]).catch((error) => { console.error(error) })
}



function mapColumn(e) {
  var {
    asks: [ask = [0]] = [[0]],
    bids: [bid = [0]] = [[0]],
    total_buy = 0,
    total_sell = 0,
  } = pairsInfo[e.s] || { asks: [[0]], bids: [[0]] }
  return {
    "MarketName": e.s.replace('_', '-'),
    "High": parseFloat(e.h),
    "Low": parseFloat(e.l),
    "BaseVolume": parseFloat(e.q),
    "Last": parseFloat(e.n),
    "TimeStamp": new Date(e.t).toISOString(),
    "Volume": e.q / e.n,
    "Ask": ask[0] || parseFloat(e.n),
    "Bid": bid[0] || parseFloat(e.n),
    "PrevDay": parseFloat(e.c),
    // "OpenBuyOrders": total_buy || 0,
    // "OpenSellOrders": total_sell || 0,
  }
}


function volumnByBaseCoin(tickers) {
  const basecoinVolumeMap = {}
  const coinVolumeMap = {}
  for (var ticker of tickers) {
    const [Coin, basedCoin] = ticker.s.split('_');

    if (!basecoinVolumeMap[basedCoin])
      basecoinVolumeMap[basedCoin] = 0;


    basecoinVolumeMap[basedCoin] += parseFloat(ticker.q);



    // if (!coinVolumeMap[Coin])
    //   coinVolumeMap[Coin] = 0;

    // coinVolumeMap[Coin] += parseFloat(ticker.q) / parseFloat(ticker.n)

    // volumesMap[Coin] += parseFloat(ticker.q);
  }

  for (var CoinName in volumeP2P) {
    if (volumeP2P[CoinName])
      basecoinVolumeMap[CoinName] += volumeP2P[CoinName].success || 0;
  }

  return Object.entries({ ...coinVolumeMap, ...basecoinVolumeMap })
    .map(([key, value]) => ({ "CoinName": key, "Volume": value }))
}

function mapData({ d = [], e = "", t = 0 } = {}) {

  return {
    success: true,
    message: "",
    result: d.map(mapColumn),
    // p2pVolume: Object
    //   .entries(volumeP2P)
    //   .map(([CoinName, { success }]) => ({ CoinName, Volume: success })),
    volumes: volumnByBaseCoin(d),
    // e,
    t,
  }
}

function mapVolume({ d = [], e = "", t = 0 } = {}) {
  return {
    volumes: volumnByBaseCoin(d),
    t,
  }
}

const excludePairs = [
  "ADT_ETH",
  "ADT_BTC",
  "AST_ETH",
  "AST_BTC",
  "ZIL_BTC",
  "ZIL_ETH",
]


/**
 * 
 * @param {WSTicker} newData 
 * @param {WSTicker} oldData 
 * @returns {WSTicker}
 */
function processSocketData(newData, oldData) {
  try {
    /**@type {WSTicker} */
    var returnData = JSON.parse(JSON.stringify(newData));

    /**@type {{[k:string]:PairTicker}} */
    var oldPairsIndex = {}

    for (let pair of (oldData.d || []))
      oldPairsIndex[pair.s] = pair;


    for (let pair of (returnData.d || [])) {
      var refpair = oldPairsIndex[pair.s]

      if (refpair && refpair.n == pair.n && refpair.q == pair.q) {
        pair.t = refpair.t;
      } else {
        pair.t = returnData.t;
      }
    }

    return {
      e : returnData.e,
      t : returnData.t,
      d : (returnData.d || [])
        .filter(data => !excludePairs.includes(data.s))
        .filter(data => !data.s.endsWith('_KNOW')),
    }
  } catch (error) {
    console.error(error)
    return newData
  }
}


var socket = new WebSocket(tickerWss)




socket.on(
  'message',
  function incoming(data) {
    console.log("update " + new Date().toISOString())
    globalData = processSocketData(JSON.parse(data), globalData)
    lastTicker = Date.now();
  }
);

socket.on(
  'error',
  function (error) {
    console.error(error)
  }
);

server.listen(port)


//Check websocket is alive and restart
setInterval(() => {
  if (Date.now() - lastTicker > 10000) {
    console.log("Reconnect to websocket");
    try {
      socket.removeAllListeners();
      socket.close();
    } catch (error) {
      
    }

    socket = new WebSocket(tickerWss)
    socket.on('message', function incoming(data) {
      console.log("update " + new Date().toISOString())
      globalData = processSocketData(JSON.parse(data), globalData)
      lastTicker = Date.now();
    });
    socket.on(
      'error',
      function (error) {console.error(error) }
    );
  }
}, 20000)

console.log("Started")



