//@ts-check
const fetch = require('node-fetch')


const timeout = 3600 * 1000
const usdtTicker = 'https://api.coinmarketcap.com/v2/ticker/825/?convert=USD'

/**
 * @type {{
    disclaimer: string,
    license: string,
    timestamp: string,
    base: string,
    rates: {[k:string]:number}
  }}
 */
const usdtData = {}

let updateTask = null;
let lastUpdate = 0;


async function getExchangeRate() {
  console.log("GET USDTDATA API");
  return fetch(usdtTicker)
    .then(e => e.json())
    .then(e => {
      Object.assign(usdtData, e)
    });
}


async function getUSDTData(req, res) {

  if (Date.now() - lastUpdate > timeout) {
    updateTask = updateTask || getExchangeRate();
    await updateTask;
    lastUpdate = Date.now();
  }
  updateTask = null;

  res.json((usdtData || {}).data || [])
}

module.exports = getUSDTData

