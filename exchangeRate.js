//@ts-check
const fetch = require('node-fetch')


const timeout = 3600 * 1000
const exchangeRateAPI = 'https://openexchangerates.org/api/latest.json?app_id=31fc7d1c1bae4b10b9a96c500bfbeadb'

/**
 * @type {{
    disclaimer: string,
    license: string,
    timestamp: string,
    base: string,
    rates: {[k:string]:number}
  }}
 */
const exchangeRateData = {}

let updateTask = null;
let lastUpdate = 0;


async function getExchangeRate() {
  console.log("GET EXCHANGE API");
  return fetch(exchangeRateAPI)
    .then(e => e.json())
    .then(e => {
      Object.assign(exchangeRateData, e)
    });
}


async function getFiatExchangeRate(req, res) {

  if (Date.now() - lastUpdate > timeout) {
    updateTask = updateTask || getExchangeRate();
    await updateTask;
    lastUpdate = Date.now();
  }
  updateTask = null;

  res.json((exchangeRateData || {}).rates || [])
}

module.exports = getFiatExchangeRate

