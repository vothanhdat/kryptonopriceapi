//@ts-check
const socketIO = require('socket.io')
const WebSocket = require('ws');

const enpoint = `wss://stream.binance.com:9443`




module.exports = http => {
  const io = socketIO(http, {
    path: '/bi/'
  });
  const inputSteamIndex = {}

  const getTradeSocketStream = function (symbol) {
    return inputSteamIndex[symbol]
      || (inputSteamIndex[symbol] = (function (symbol) {
        const wsLink = `${enpoint}/ws/${symbol}@depth`
        var socket = new WebSocket(wsLink)
        var lastUpdate = Date.now();
        var processMsg = function (rawdata) {
          try {
            console.log(`on Update : depth@${symbol}`)
            io.sockets.in(`depth@${symbol}`).emit('message', rawdata)
            lastUpdate = Date.now();
          } catch (error) {
            console.error(error)
          }
        };
        socket.on("message", processMsg);

        socket.on("open", function () {
          console.log("OPENED");
        });
        socket.on('error', function (error) {
          console.error(error)
        });

        setInterval(() => {
          if (Date.now() - lastUpdate > 10000) {
            lastUpdate = Date.now();
            console.log("Reconnect to websocket");
            try {
              socket.removeAllListeners();
              socket.close();
            } catch (error) { 
              console.error(error)
            }
            socket = new WebSocket(wsLink)
            socket.on("message", processMsg);
            socket.on('error', function (error) {
              console.error(error)
            });
          }
        }, 1000)

        console.log('Created new socket', symbol, wsLink)

        return socket
      })(symbol))
  }

  io.on('connection', function (socket) {
    let symbol = socket.handshake.query.symbol;
    getTradeSocketStream(symbol);
    socket.join(`depth@${symbol}`);
  });

}