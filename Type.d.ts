
declare interface PairTicker {
  q: string;
  s: string;
  c: string;
  pc: string;
  t: any;
  ch: string;
  h: string;
  l: string;
  n: string;
  o: string;
}

declare interface WSTicker {
  t: number;
  d: PairTicker[];
  e: string;
}


declare interface PairInfo {
  asks: [number, number][],
  bids: [number, number][],
  total_buy: number,
  total_sell: number,
}

