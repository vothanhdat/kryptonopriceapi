const fetch = require('node-fetch')
const HmacSHA256 = require('crypto-js/hmac-sha256')
const Base16 = require('crypto-js/enc-hex')

// process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const apiPath = 'https://xapi.kryptono.exchange/k/papi/v1/enterprise/user-payment-detail'

/**
 * @param {Request} req 
 * @param {Response} res 
  */
async function getPaymentData(req, res) {
  // console.log(JSON.stringify(req.body))
  console.log(Base16.stringify(HmacSHA256(
    JSON.stringify(req.body),
    's5H49ZZw/7oD8zytj2+aHe8FTCsuhMuDvye6WoIihoI='
  )))
  return fetch(apiPath, {
    method: "POST",
    headers: {
      "X-KRP-APIKEY": `eyJhbGciOiJIUzUxMiJ9.eyJjcmVhdGVkQXQiOjE1MzY4MTE4NDkxNzgsInN1YiI6IiIsImVudGVycHJpc2VJRCI6IjFjNzJjMzNlLTJhNGYtNDBiOC04NjA3LTI0OGM1ODAxYmFkOSIsInR5cGUiOiJzZXJ2ZXJLZXkiLCJleHAiOjE3MzA5Mzc2MDAwLCJpYXQiOjE1MzY4MTE4NDl9.HAJqxHngi9SBl418xzbv-gyreXVoWz4gUJiA5mD-LSFtZOoEIHAvKgeutXvkfksGkT5MAyBS0JLZLAiKiXPmQQ`,
      // "X-KRP-APIKEY": `eyJhbGciOiJIUzUxMiJ9.eyJjcmVhdGVkQXQiOjE1MzY4MDk5MDYzOTIsInN1YiI6IiIsImVudGVycHJpc2VJRCI6ImVlOThlYzc3LTUwMmEtNDZlZi05YjQ0LTYxOGU1YjgyNjE0NCIsInR5cGUiOiJzZXJ2ZXJLZXkiLCJleHAiOjE3MzA5Mzc2MDAwLCJpYXQiOjE1MzY4MDk5MDZ9.6ef8nPAbQxtoT5brLl6mlr5p_9FKi7lgZt_CCZ7PlnZr6e8utBBFRHJLj8xSsWYOIJpPI9bFPZaRt2JFYe0RUA`,
      "Checksum": Base16.stringify(HmacSHA256(
        JSON.stringify(req.body),
        's5H49ZZw/7oD8zytj2+aHe8FTCsuhMuDvye6WoIihoI='
        // 's5H49ZZw/7oD8zytj2+aHe8FTCsuhMuDvye6WoIihoI='
      )),
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Cache-Control': 'no-cache'
    },
    body:JSON.stringify(req.body)
  })
    .then(e => e.json())
    .then(e => res.json(e))
    .catch(e => {
      console.error(e);
      res.status(404);
      return res.json(e);
    });
}

module.exports = getPaymentData

