//@ts-check
const fetch = require('node-fetch')
const URL = require('url')


const timeout = 1 * 1000

/**
 * @type {{[k:string]:{data:any,task:Promise,last:number}}}
 */
const dataIdx = {}


async function getDepthSnapshot(symbol = '') {
  console.log("GET USDTDATA API");

  return fetch(`https://www.binance.com/api/v1/depth?symbol=${symbol.toUpperCase()}&limit=50`)
    .then(e => e.json())
    .then(e => {
      Object.assign(dataIdx[symbol], {
        data: e,
        last: Date.now(),
      })
    });
}

/**
 * 
 * @param {Request} req 
 * @param {Express.Response} res 
  */
async function getData(req, res) {
  var query = URL.parse(req.url, true).query;
  var symbol = query.symbol || ''
  if (!dataIdx[symbol])
    dataIdx[symbol] = {};
  var data = dataIdx[symbol];
  if (Date.now() - (data.last || 0) > timeout) {
    await (data.task || (data.task = getDepthSnapshot(symbol)));
    data.task = null;
  }
  res.json(data.data || {})
}

module.exports = getData

